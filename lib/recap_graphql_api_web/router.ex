defmodule RecapGraphqlApiWeb.Router do
  use RecapGraphqlApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", RecapGraphqlApiWeb do
    pipe_through :api
  end

  forward "/graphiql",
  Absinthe.Plug.GraphiQL,
  schema: RecapGraphqlApiWeb.Schema,
  interface: :simple
end
