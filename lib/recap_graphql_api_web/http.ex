defmodule RecapGraphqlApiWeb.Http do
    
    defp host do
        Application.get_env(:recap_graphql_api, :app_vars)[:recap_api_host]
      end

    def user_id do
        Application.get_env(:recap_graphql_api, :app_vars)[:user_id]
      end
    
      defp port do
        Application.get_env(:recap_graphql_api, :user_id)
      end
    
      def post(url, params) do
        url = host <> url 
        IO.puts url
        IO.puts Poison.encode!(%{title: "TT"})
        req = HTTPoison.post(url, Poison.encode!(%{"title": "TT"}), %{"Content-Type" => "application/json"}) 
        # case HTTPoison.post(url, %{body: "SUPERTITLE"}) do
        #   {:ok, %HTTPoison.Response{body: body}} ->
        #     {:ok, body |> Poison.decode!}
        #   {:error, %HTTPoison.Error{reason: reason}} ->
        #     {:error, reason}
        # end
      end
    
      def get(url) do
        url = host <> url 
        IO.puts url
        case HTTPoison.get(url) do
          {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
            {:ok, body |> Poison.decode!}
          {:error, %HTTPoison.Error{reason: reason}} ->
            {:error, reason}
        end
      end
end