defmodule RecapGraphqlApiWeb.Schema do
  use Absinthe.Schema

  alias RecapGraphqlApiWeb.Resolvers.QueueResolvers
  alias RecapGraphqlApiWeb.Resolvers.FeedResolvers
  alias RecapGraphqlApiWeb.Resolvers.UserResolvers
  alias RecapGraphqlApiWeb.Resolvers.TicketResolvers

  object :queue do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :icon_id, non_null(:id)
  end

  object :user do
    field :id, non_null(:id)
  end

  object :ticket do
    field :id, non_null(:id)
  end

  object :feed_item do
    field :id, non_null(:id)
    field :title, non_null(:string)
    field :ticket, :ticket do
        resolve &TicketResolvers.get_ticket/3
    end
    field :author, non_null(:user) do
        resolve &UserResolvers.get_user/3
    end
  end

  query do
    field :queue_by_id, non_null(:queue) do
      arg :id, non_null(:id)
        resolve &QueueResolvers.get_queue/3
    end
    field :queues, non_null(list_of(non_null(:queue))) do
        resolve &QueueResolvers.get_queues/3
    end
    field :author, non_null(:queue) do
        resolve &UserResolvers.get_user/3
    end
    field :feed_items, non_null(list_of(non_null(:feed_item))) do
      arg :queue_id, non_null(:id)
        resolve &FeedResolvers.get_feed_items/3
    end
  end

  mutation do
    field :create_feed_item, :feed_item do
      arg :queue_id, non_null(:id)
      arg :title, non_null(:string)
  
        resolve &FeedResolvers.create_feed_item/3
    end
  end


end