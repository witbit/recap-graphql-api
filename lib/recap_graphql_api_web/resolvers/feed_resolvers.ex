defmodule RecapGraphqlApiWeb.Resolvers.FeedResolvers do

    alias RecapGraphqlApiWeb.Http

    
    def map_keys_to_atoms(map) do
        for {key, val} <- map, into: %{}, do: {String.to_atom(key), val}
      end

    def get_feed_items(_root, %{queue_id: queue_id}, _info) do
        user_id = Http.user_id()
        url = "/api/v5/teacher/" <> user_id <> "/classroom/" <> queue_id <> "/feed/joined?join_entities=tickets,ticket_assignments,questions,journeys,journey_assignments,journey_steps,reels,reel_comments,reel_stats,student_questions,user_profiles,feed_item_filtered_archived"
        case Http.get(url) do
            {:ok, body} ->
                {:ok, Enum.map(body["feed_items"]["items"], &map_keys_to_atoms/1)}
            {:error, reason} ->
                {:error, "ERROR"}
          end 
    end

    def create_feed_item(_root, args, _info) do
        IO.puts "TTTTTTTTTTTT"
        queue_id = args.queue_id
        title = args.title
        user_id = Http.user_id()
        url = "/api/v5/teacher/" <> user_id <> "/classes/" <> queue_id <> "/append/activity"
        response = Http.post(url, %{title: title})
        # case Http.post(url, %{title: title}) do
        #     {:ok, body} ->
        #         {:ok, map_keys_to_atoms(body)}
        #     {:error, reason} ->
        #         {:error, "ERROR"}
        #   end 
        IO.inspect response
        {:ok, %{id: 1}}
    end

end