defmodule RecapGraphqlApiWeb.Resolvers.UserResolvers do

    alias RecapGraphqlApiWeb.Http

    
    def map_keys_to_atoms(map) do
        for {key, val} <- map, into: %{}, do: {String.to_atom(key), val}
      end

    def get_user(%{ticket_id: ticket_id}, args, _info) do
        IO.inspect ticket_id
        {:ok, %{id: 55}}
    end

    def get_user(%{student_question_id: ticket_id}, args, _info) do
        IO.inspect ticket_id
        {:ok, %{id: 5}}
    end

    def get_user(root, args, _info) do

        IO.inspect root
        {:ok, %{id: 56}}
    end


end