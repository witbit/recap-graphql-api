defmodule RecapGraphqlApiWeb.Resolvers.QueueResolvers do

    alias RecapGraphqlApiWeb.Http

    def get_queue(_root, %{id: id}, _info) do
        user_id = Http.user_id()
        url = "/api/v5/users/" <> user_id <> "/classes/" <> id 
        case Http.get(url) do
            {:ok, body} ->
                {:ok, map_keys_to_atoms(body)}
            {:error, reason} ->
                {:error, "ERROR"}
          end 
    end

    def map_keys_to_atoms(map) do
        for {key, val} <- map, into: %{}, do: {String.to_atom(key), val}
      end

    def get_queues(_root, _args, _info) do
        user_id = Http.user_id()
        url = "/api/v5/teacher/" <> user_id <> "/classes"
        case Http.get(url) do
            {:ok, body} ->
                {:ok, Enum.map(body["items"], &map_keys_to_atoms/1)}
            {:error, reason} ->
                {:error, "ERROR"}
          end 
    end

end