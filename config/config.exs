# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :recap_graphql_api, RecapGraphqlApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "nYofiaRuC0tc/QPreyZ4d84Xp7E7ITRKQcLvArgpeSgBv+z+8R/v2pcFoLW71+AJ",
  render_errors: [view: RecapGraphqlApiWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: RecapGraphqlApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :recap_graphql_api, :app_vars,
recap_api_host: System.get_env("RECAP_API_HOST"),
user_id: System.get_env("USER_ID")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
